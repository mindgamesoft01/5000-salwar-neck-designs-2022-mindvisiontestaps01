package com.testapshd.salwarneckdesigns.collection.hd.new2019

 data class Item(
         val topic_title:String = "Topic Title",
         val topic_icon:String = "",
         val menus:ArrayList<String>
  )
